#!/usr/bin/env python

import os
import sys
import re
import shutil
import http.client
import time
import gitlab
import optparse
import subprocess
import json
from junit_xml import TestSuite, TestCase
import mmap
import ssl

if __name__ != "__main__":
    exit(1)

def pname():
    pid = os.getpid()
    return "[gitleaks-scan," + str(pid) + "]"

def splunk_push_report(payload):
    '''Send gitleaks report (findings) to splunk'''
    print(pname() + " sent gitleaks secrets")
#    connection = http.client.HTTPSConnection('my-splunk-server.net', 8088, timeout=10, context = ssl.#_create_unverified_context())
#    headers = {'Authorization': 'Splunk ' + os.getenv('SF_SPLUNK_HEC_TOKEN')}
#    connection.request("POST", '/services/collector/event', payload, headers)
#    response = connection.getresponse()
#    print(pname() + ' - splunk push response: ' + response.read().decode())
#    connection.close()

parser = optparse.OptionParser("usage: %prog [options]")

parser.add_option(
    "-u",
    "--url",
    dest="url",
    default="https://gitlab.com",
    type="string",
    help="base URL of the GitLab instance",
)

parser.add_option(
    "-l", "--login", dest="login", default="gitlab-ci-token", type="string", help="GitLab username"
)

parser.add_option(
    "-t", "--token", dest="token", default="", type="string", help="GitLab token"
)

parser.add_option(
    "-n",
    "--namespace",
    dest="namespace",
    default="",
    type="string",
    help="namespace to clone",
)

parser.add_option(
    "--disable-root",
    action="store_true",
    dest="noroot",
    default=False,
    help="do not create root namepace folder in path",
)

(options, args) = parser.parse_args()

git_path = shutil.which("git")
if git_path == "None":
    sys.stderr.write("Error: git executable not installed or not in $PATH" + "\n")
    exit(1)
else:
    print(pname() + " using " + git_path)

t = time.time()

gl = gitlab.Gitlab(options.url, options.token)

group = gl.groups.get(options.namespace, lazy=True, include_subgroups=True)

projects = []

# Get all projects inside the namespace
for project in group.projects.list(all=True):
    projects.append(project)
    print(pname() + " found " + project.path_with_namespace)


# Get all projects inside the subgroups
for group in gl.groups.list(
    all=True, owned=True, query_parameters={"id": options.namespace}
):

    for project in group.projects.list(all=True):
        projects.append(project)
        print(pname() + " found " + project.path_with_namespace)

    subgroups = group.subgroups.list(all=True)
    while True:
        for subgroup in subgroups:

            real_group = gl.groups.get(subgroup.id, lazy=True)

            for project in real_group.projects.list(all=True):
                projects.append(project)
                print(pname() + " found " + project.path_with_namespace)

            subgroups = real_group.subgroups.list(all=True)

            if len(subgroups) == 0:
                next

        if len(subgroups) == 0:
            break

    test_suites = []
    splunk_events = '{"sourcetype": "secret-scan", "event": "scan_prj='+ os.getenv('CI_PROJECT_PATH') + '"}'
    for project in projects:
        print(pname() + " secret scanning project " + project.path_with_namespace)
        folders = [f.strip().lower() for f in project.path_with_namespace.split("/")]
        if options.noroot:
            folders.remove(options.namespace)

        json_report=str(project.id) + "_" + project.path + ".json"

        print(pname() + " repo " + project.http_url_to_repo.replace('.git',''))
        subprocess.run(["gitleaks", "--repo-url=" + project.http_url_to_repo.replace('.git',''), 
            "--username="+options.login, "--password=" + options.token, 
            "-v", "--report=" + json_report])
        
        test_cases = []
        if os.path.isfile(json_report):
            with open(json_report) as json_file:
                datas = json.load(json_file)
                for data in datas:

                    testcase = TestCase(name= data['rule']+ " :: " + data['leakURL'], 
                            classname=project.path_with_namespace, 
                            file=data['file'],
                            line=data['lineNumber'])

                    findingOutput = "### " + data['rule'] + " ###" + "\n" + \
                        "== offender: " + data['offender'] + "\n" + \
                        "== leakURL: " + data['leakURL'] + "\n" + \
                        "== commit: " + data['commit'] + "\n" + \
                        "== author: " + data['author'] + "\n" + \
                        "== email: " + data['email'] + "\n" + \
                        "== date: " + data['date'] + "\n" + \
                        "== commitMessage: " + data['commitMessage']  + "\n" + \
                        "== line: " + data['line']
                        
                    testcase.add_failure_info(message="secret: "+data['offender'], output=findingOutput, failure_type='failure')
                    test_cases.append(testcase)

                    # add to splunk events to push
                    splunk_events += '{"sourcetype": "secret", "event": "scan_prj='+ os.getenv('CI_PROJECT_PATH') + ',project='+project.path_with_namespace + ',commit='+data['commit'] + ',leakURL='+data['leakURL'] + ',rule=\\"'+data['rule'] + '\\",date=' + data['date'] + ',author=' + data['author'] + ',email=' + data['email'] + '"}'

            test_suites.append(TestSuite(project.path_with_namespace, test_cases))

    # junitxml report  
    with open('junitxml.xml', 'w') as f:
        print(pname() + " write junitxml.xml")
        TestSuite.to_file(f, test_suites, prettyprint=True)
    
    # push splunk_events to splunk
    splunk_push_report(splunk_events)

print(pname() + " mission accomplished in " + str(round(time.time() - t, 2)) + "s")
exit(0)
