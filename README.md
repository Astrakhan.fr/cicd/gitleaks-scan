# gitleaks-scan

## Purpose

Scan for secret all GitLab projects of a GitLab Group

## How-To

- Fork this project
- Go to CI/CD > Piplines and click on `Run Pipeline`

![1_run_pipeline](img/1_run_pipeline.png)

- Set the variables `GITLAB_TOKEN` and `GITLAB_NAMESPACE` with respectively a `GitLab Personal Access Token` with `api` scope and the namespace to scan

- Once the pipeline ends, go to the `Tests` tab to see your report

![2_gitleaks_report](img/2_gitleaks_report.png)

![3_gitleaks_report](img/3_gitleaks_report.png)

![4_gitleaks_report](img/4_gitleaks_report.png)

## Splunk monitoring

The foundings can be sent to splunk (or another monitoring tools to implement) by modifing the python script. There is already a [`splunk_push_report`](https://gitlab.com/Astrakhan.fr/cicd/gitleaks-scan/-/blob/master/gitleak-scan.py#L26) function for this purpose.
